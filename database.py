from pony.orm import *

from datetime import datetime
from uuid import UUID
import os

DATABASE_URL = os.environ.get("DATABASE_URL", default="localhost")
DB_USER = os.environ.get("DB_USER", default="gql_stub")
DB_PASSWORD = os.environ.get("DB_PASSWORD", default="gql_stub_postgres")

db = Database()
db.bind(provider='postgres', user=DB_USER, password=DB_PASSWORD, host=DATABASE_URL, database='postgres')
# db.bind(provider='sqlite', filename='database.sqlite', create_db=True)


# This table for all types declaration like 'input' or 'type' in GraphQL
# Save all types except Query Mutation and Subscription types
class Type(db.Entity):
    id = PrimaryKey(UUID, auto=True)
    title = Optional(str, unique=True, nullable=True)
    graphql_code = Required(str, unique=True, nullable=False)
    comment = Required(str, unique=False, nullable=False)
    creation_time = Required(datetime, nullable=False)
    last_change_time = Required(datetime, nullable=False)


# This enum should be used in Query class
class QueryTypeEnum:
    QUERY = "query"
    MUTATION = "mutation"

    @staticmethod
    def from_string(line: str):
        if type(line) == QueryTypeEnum:
            return line

        match line.strip().lower():
            case "query":
                return QueryTypeEnum.QUERY
            case "mutation":
                return QueryTypeEnum.MUTATION
            case _:
                return None


class Query(db.Entity):
    """
        This table for all request fields of Query or Mutations type
        for example if you want this:

        type Query{
            getUser(id: Int): User
        }

        You should save only getUser(id: Int): User
    """
    id = PrimaryKey(UUID, auto=True)
    title = Optional(str, unique=True, nullable=True)
    type = Required(str, unique=False, nullable=False)

    # line_def is only line from Query type def like: userById(id: Int): User
    query_line = Required(str, unique=True, nullable=False)
    comment = Required(str, unique=False, nullable=False)
    creation_time = Required(datetime, nullable=False)
    last_change_time = Required(datetime, nullable=False)
    stubs = Set('QueryStub', cascade_delete=True)


class QueryStub(db.Entity):
    id = PrimaryKey(UUID, auto=True)

    # input_values - json with query arg name key and input for query arg value
    input_values = Required(Json, unique=False, nullable=False)

    # output_json - full output json answer with nested json (full json)
    output_json = Required(Json, unique=False, nullable=False)

    query_id = Required('Query')


db.generate_mapping(create_tables=True)
