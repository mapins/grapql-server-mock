from typing import List, Any, Tuple, Generator
from uuid import UUID
from datetime import datetime

from pony.orm import db_session, select

from ariadne import gql, ObjectType, make_executable_schema, graphql_sync
from graphql import GraphQLSchema, GraphQLResolveInfo

from database import Type, Query, QueryStub, QueryTypeEnum


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class _QueryWrapper:
    def __init__(self, query_type: str, queries_handler):
        self._query = ObjectType(query_type)
        self._type: str = query_type
        self._row_fields: dict[UUID:str] = {}
        self._queries_handler = queries_handler

    def add_fields(self, fields: List[Tuple[UUID, str]]) -> None:
        for val in fields:
            self.add_field(*val)

    def add_field(self, query_id: UUID, query_line: str) -> None:
        self._row_fields[query_id] = query_line
        field = query_line.split("(")[0].strip()
        self._query.set_field(field,
                              lambda *args, **kwargs: self._queries_handler(query_id, *args, **kwargs))

    def get_raw_queries(self):
        return self._row_fields.values()

    def get_query_object(self) -> ObjectType:
        return self._query

    def clean(self) -> None:
        self._query = ObjectType(self._type)
        self._row_fields = {}

    def delete_field(self, id: UUID) -> str:
        field = self._row_fields.pop(id, None)
        if field:
            resolver_name_extraction = field.split('(')[0]
            self._query._resolvers.pop(resolver_name_extraction)
        return field

    def __str__(self):
        if len(self._row_fields) > 0:
            return "\ntype " + self._type + "{ \n\t" + \
                "\n\t".join(self.get_raw_queries()) + \
                "\n}\n"
        else:
            return ""


class GraphQLSchemaService(metaclass=Singleton):

    def __init__(self):
        self._graphql_schema: GraphQLSchema = None

        self._query: _QueryWrapper = _QueryWrapper("Query", self.__query_common_handler)
        self._mutation: _QueryWrapper = _QueryWrapper("Mutation", self.__query_common_handler)

    @db_session
    def load_graphql_schema_from_db(self):
        self._query.clean()
        self._mutation.clean()

        # get all queries definitions in nake format (without type declaration)
        queries_list = select((val.id, val.query_line)
                              for val in Query if val.type == QueryTypeEnum.QUERY)[:]
        self._query.add_fields(queries_list)
        mutations_list = select(
            (val.id, val.query_line) for val in Query if val.type == QueryTypeEnum.MUTATION)[:]
        self._mutation.add_fields(mutations_list)

        self._update_schema()

    def __get_all_types_schema(self):
        return "\n".join([val.graphql_code for val in Type.select()])

    def get_full_schema_str(self) -> str:
        return self.__get_all_types_schema() + "\n" + str(self._query) + "\n" + str(self._mutation)

    def sync_eval_graphql(self, data, **kwargs):
        if self._graphql_schema is None:
            if len(self._query.get_raw_queries()) == 0:
                raise ValueError("GraphQL schema is not full, nothing queries were detected")
            raise ValueError("GraphQL schema not configured yet")

        return graphql_sync(
            self._graphql_schema,
            data,
            **kwargs
        )

    @db_session
    def get_type(self, type_id: UUID = None, time_from: datetime = None, time_to: datetime = None):
        if type_id is not None:
            return Type[type_id].to_dict()
        else:
            sort_fun = True
            if time_from is not None:
                time_from = time_from.replace(tzinfo=None)
                sort_fun = lambda val: val >= time_from
            if time_to is not None:
                time_to = time_to.replace(tzinfo=None)
                sort_fun = lambda val: sort_fun and (val <= time_to)

            if sort_fun is not True:
                return [val.to_dict(with_collections=True) for val in Type.select() if sort_fun(val.last_change_time)]
            return [val.to_dict(with_collections=True) for val in Type.select()]

    @db_session
    def add_type(self, graphql_code: str = "", **kwargs):
        graphql_code = gql(graphql_code)
        time = datetime.now()
        try:
            self._update_schema(graphql_code)
        except Exception as e:
            raise e
        else:
            new_type = Type(graphql_code=graphql_code, creation_time=time, last_change_time=time, **kwargs)
            return new_type.to_dict()

    @db_session
    def update_type(self, type_id: UUID, **kwargs):

        kwargs.pop("creation_time", None)
        kwargs.pop("last_change_time", None)

        old_type = Type[type_id]
        Type[type_id].set(last_change_time=datetime.now(), **kwargs)
        if kwargs.get("scheme", False):
            try:
                self._update_schema()
            except Exception as e:
                Type[type_id] = old_type
                raise e
        return Type[type_id].to_dict()

    @db_session
    def delete_type(self, type_id: UUID):
        Type[type_id].delete()
        self._update_schema()

    @db_session
    def get_stub(self, stub_id: UUID = None, query_id: UUID = None):
        if stub_id is not None:
            return QueryStub[stub_id].to_dict()
        elif query_id is not None:
            return [val.to_dict() for val in Query[query_id].stubs]
        else:
            return [val.to_dict() for val in QueryStub.select()]

    @db_session
    def add_stub(self, query_id: UUID, input_values: dict, output_json: dict, **kwargs):

        query = Query[query_id]
        stub = QueryStub(query_id=query, input_values=input_values, output_json=output_json, **kwargs)
        return stub.to_dict()

    @db_session
    def update_stub(self, stub_id: UUID, **kwargs):
        QueryStub[stub_id].set(**kwargs)
        return QueryStub[stub_id].to_dict()

    @db_session
    def delete_stub(self, stub_id: UUID):
        QueryStub[stub_id].delete()

    @db_session
    def get_query(self, query_id: UUID = None, time_from: datetime = None, time_to: datetime = None):
        if query_id is not None:
            return Query[query_id].to_dict(with_collections=True)
        else:
            sort_fun = True
            if time_from is not None:
                time_from = time_from.replace(tzinfo=None)
                sort_fun = lambda val: val >= time_from
            if time_to is not None:
                time_to = time_to.replace(tzinfo=None)
                sort_fun = lambda val: sort_fun and (val <= time_to)

            if sort_fun is not True:
                return [val.to_dict(with_collections=True) for val in Query.select() if sort_fun(val.last_change_time)]
            return [val.to_dict(with_collections=True) for val in Query.select()]

    @db_session
    def add_query(self, type: str, query_line: str = "", **kwargs):
        ql_query_type = QueryTypeEnum.from_string(type)

        if ql_query_type is None:
            raise ValueError("Undefined type of query")

        # simple graphql syntax check
        gql("type Query { \n" + query_line + "\n}")

        check_UUID = UUID(int=0)
        try:
            # this identifier special for test case
            # we have a types check thus nothing changes if Mutation begin Query type on testing moment
            self._query.add_field(check_UUID, query_line)
            self._update_schema(with_handlers=False)
            self._query.delete_field(check_UUID)
        except Exception as e:
            self._query.delete_field(check_UUID)
            raise e
        else:
            time = datetime.now()
            new_query = Query(query_line=query_line, type=ql_query_type,
                              creation_time=time, last_change_time=time, **kwargs)
            match ql_query_type:
                case QueryTypeEnum.QUERY:
                    self._query.add_field(new_query.id, query_line)
                case QueryTypeEnum.MUTATION:
                    self._mutation.add_field(new_query.id, query_line)
            self._update_schema()

            return new_query.to_dict()

    @db_session
    def update_query(self, query_id: UUID, **kwargs):
        query = Query[query_id]

        kwargs.pop("creation_time", None)
        kwargs.pop("last_change_time", None)
        kwargs.pop("stubs", None)

        if kwargs.get("type", False):
            gql_type = QueryTypeEnum.from_string(kwargs["type"])
            if gql_type is not None:
                kwargs["type"] = gql_type
                if gql_type != query.type:
                    if gql_type == QueryTypeEnum.QUERY:
                        field = self._mutation.delete_field(query_id)
                        self._query.add_field(query_id, field)
                    else:
                        field = self._query.delete_field(query_id)
                        self._mutation.add_field(query_id, field)

            else:
                raise ValueError("The query type setted but field value is not correct")

        Query[query_id].set(last_change_time=datetime.now(), **kwargs)
        self._update_schema()

        return Query[query_id].to_dict()

    @db_session
    def delete_query(self, query_id: UUID):
        query = Query[query_id]
        query_type = query.type
        query.delete()

        if query_type == QueryTypeEnum.QUERY:
            self._query.delete_field(query_id)
        else:
            self._mutation.delete_field(query_id)
        self._update_schema()

    def _update_schema(self, addiction="", with_handlers: bool = True):
        full_schema_text = self.get_full_schema_str() + (f"\n{addiction}\n" if addiction else "")

        if len(self._query.get_raw_queries()) == 0:
            return

        if with_handlers:
            if len(self._mutation.get_raw_queries()):
                self._graphql_schema = make_executable_schema(full_schema_text,
                                                              self._query.get_query_object(),
                                                              self._mutation.get_query_object())
            else:
                self._graphql_schema = make_executable_schema(full_schema_text,
                                                              self._query.get_query_object())
        else:
            self._graphql_schema = make_executable_schema(full_schema_text)

    def __query_common_handler(self, query_id: UUID, obj: Any, info: GraphQLResolveInfo, **kwargs):
        query = Query[query_id]
        db_query = [stub for stub in query.stubs if stub.input_values == kwargs]

        if len(db_query) == 0:
            raise ValueError(f"Undefined stub for query {query.id}({query.query_line}) with values {kwargs}")
        elif len(db_query) > 1:
            raise ValueError(f"Plural stubs {[val.id for val in db_query]} "
                             f"for query {query.id}({query.query_line}) was found. "
                             f"Please resolve collision and try again.")

        return db_query[0].output_json

    @staticmethod
    def instance():
        return GraphQLSchemaService()
