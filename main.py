import json
from http.client import HTTPException

from flask import Flask, send_from_directory, make_response
from pony.flask import Pony
from graphql_schema_service import GraphQLSchemaService
from graphql_schema_settings_blueprint import graphql_settings_blueprint
from graphql_blueprint import graphql_blueprint
from flask_swagger_ui import get_swaggerui_blueprint
import os

# Flask app creation
app: Flask = Flask(__name__)

# Swagger config
SWAGGER_URL = '/dev/api/swagger'  # URL for exposing Swagger UI (without trailing '/')
OPENAPI_URL = os.environ.get("OPENAPI_URL", default='https://dev.mapins.cloud/dev/api/openapi-spec')

# Flask config
app.config.update(dict(
    DEBUG=False,
    SECRET_KEY='secret',
    CORS_HEADERS='Content-Type'
))

# Flask configure by Pony ORM, adding db session handlers
Pony(app)

swagger_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    OPENAPI_URL,
    config={  # Swagger UI config overrides
        'app_name': "GraphQl mock"
    }
)

# load and init schema from db
GraphQLSchemaService().load_graphql_schema_from_db()

app.register_blueprint(graphql_settings_blueprint)
app.register_blueprint(graphql_blueprint)
app.register_blueprint(swagger_blueprint)


@app.errorhandler(Exception)
def error_handler(e):
    err_code = e.code if type(e) == HTTPException else 422
    response = make_response()
    response.data = json.dumps({
        "code": err_code,
        "description": str(type(e)) + ": " + str(e)
    }
    )
    response.content_type = "application/json"
    print(str(type(e)) + ": " + str(e))
    return response, err_code


@app.route("/dev/api/openapi-spec", methods=["GET"])
def api_source():
    return send_from_directory("./", "openapi.yaml")


if __name__ == "__main__":
    app.run()
