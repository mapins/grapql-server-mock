from datetime import datetime

import dateutil.parser
from flask.blueprints import Blueprint
from flask import request, jsonify, abort
from uuid import UUID
from graphql_schema_service import GraphQLSchemaService

graphql_settings_blueprint = Blueprint("graphql_settings_blueprint", __name__, url_prefix='/dev/api/graphqlMock')

schema_service = GraphQLSchemaService()


def _get_datetime_from_str(string: str) -> datetime | None:
    if string is None:
        return None
    return dateutil.parser.parse(string)


@graphql_settings_blueprint.route("/type", methods=["POST"])
def add_type():
    json: dict | list = request.get_json()

    # if id in JSON object updates in other way creates
    if type(json) == list:
        abort(422, "JSON should be object")
    # if id in JSON object updates in other way creates
    if json.get("id", False):
        key = UUID(json.pop("id"))
        if key != UUID(int=0):
            return schema_service.update_type(key, **json), 200
    elif not json.get("graphql_code", False):
        abort(422, "graphql_code field does not exist")
    return schema_service.add_type(**json), 200


# there will be two REST query parameters from and to both are time
@graphql_settings_blueprint.route("/type", methods=["GET"])
def get_all_types():
    time_from = _get_datetime_from_str(request.args.get("from", default=None, type=str))
    time_to = _get_datetime_from_str(request.args.get("to", default=None, type=str))

    return schema_service.get_type(time_from=time_from, time_to=time_to), 200


@graphql_settings_blueprint.route("/type/<uuid:type_id>", methods=["GET"])
def get_type(type_id):
    return schema_service.get_type(type_id)


@graphql_settings_blueprint.route("/type/<uuid:type_id>", methods=["DELETE"])
def del_type(type_id):
    schema_service.delete_type(type_id)
    return "OK", 200


@graphql_settings_blueprint.route("/query", methods=["POST"])
def add_query():
    json: dict | list = request.get_json()

    if type(json) == list:
        abort(422, "JSON should be object")
    # if id in JSON object updates in other way creates
    if json.get("id", False):
        key = UUID(json.pop("id"))
        if key != UUID(int=0):
            return schema_service.update_query(key, **json), 200
    elif not json.get("query_line", False):
        abort(422, "query_line field does not exist")
    return schema_service.add_query(**json), 200


@graphql_settings_blueprint.route("/query", methods=["GET"])
def get_all_query():
    time_from = _get_datetime_from_str(request.args.get("from", default=None, type=str))
    time_to = _get_datetime_from_str(request.args.get("to", default=None, type=str))

    return schema_service.get_query(time_from=time_from, time_to=time_to)


@graphql_settings_blueprint.route("/query/<uuid:query_id>", methods=["GET"])
def get_query(query_id):
    return schema_service.get_query(query_id)


@graphql_settings_blueprint.route("/query/<uuid:query_id>", methods=["DELETE"])
def del_query(query_id):
    schema_service.delete_query(query_id)
    return "OK", 200


@graphql_settings_blueprint.route("/stub", methods=["POST"])
def add_stub():
    json: dict | list = request.get_json()

    if type(json) == list:
        abort(422, "JSON should be object")
    # if id in JSON object updates in other way creates
    if json.get("id", False):
        key = UUID(json.pop("id"))
        if key != UUID(int=0):
            return schema_service.update_stub(key, **json), 200
    # bool check for skip objects with no value
    elif not json.get("input_values", False) and type(json.get("input_values", False)) == bool:
        abort(422, "input_values field does not exist")
    elif not json.get("output_json", False) and type(json.get("input_values", False)) == bool:
        abort(422, "output_json field does not exist")
    elif not json.get("query_id", False):
        abort(422, "query_id field does not exist")
    else:
        json["query_id"] = UUID(json["query_id"])
    return schema_service.add_stub(**json), 200


@graphql_settings_blueprint.route("/stub", methods=["GET"])
def get_all_stubs():
    query_id = request.args.get("query_id", default=None, type=UUID)
    return schema_service.get_stub(query_id=query_id), 200


@graphql_settings_blueprint.route("/stub/<uuid:stub_id>", methods=["GET"])
def get_stub(stub_id):
    return schema_service.get_stub(stub_id)


@graphql_settings_blueprint.route("/stub/<uuid:stub_id>", methods=["DELETE"])
def del_stub(stub_id):
    schema_service.delete_stub(stub_id)
    return "OK", 200


@graphql_settings_blueprint.route("/schema", methods=["GET"])
def get_full_schema():
    return schema_service.get_full_schema_str(), 200
