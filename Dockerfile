FROM python:3.10-slim-bullseye
WORKDIR /app
COPY . .
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
CMD [ "gunicorn" , "-b", "0.0.0.0:5000", "-w", "1", "main:app"]
