from flask.blueprints import Blueprint
from flask import request, jsonify
from graphql_schema_service import GraphQLSchemaService

graphql_blueprint = Blueprint("graphql_blueprint", __name__, url_prefix="/dev/api")

schema_service = GraphQLSchemaService()


@graphql_blueprint.route("/graphql", methods=["POST"])
def graphql_server():
    data = request.get_json()
    success, result = schema_service.sync_eval_graphql(
        data,
        context_value=request
    )
    return jsonify(result), 200 if success else 400
